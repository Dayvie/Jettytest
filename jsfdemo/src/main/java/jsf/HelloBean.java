package jsf;

import java.io.Serializable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class HelloBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	
	public HelloBean(){
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String sayHello(){
		return "Hello";
	}
}
