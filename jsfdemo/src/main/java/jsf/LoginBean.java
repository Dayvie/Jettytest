package jsf;

import java.io.Serializable;

import controller.LoginController;


public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private LoginController loginController;

	private boolean isLoggedIn;
	
	public LoginBean(){
	}
	
	public LoginController getLoginController() {
		return loginController;
	}

	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}
	
	public void login(String user, String password){
		if(loginController.authenticate(user, password)){
			isLoggedIn = true;
		}
	}
	
	public String getUserName(){
		return loginController.getUserName();
	}
	
	public boolean isLoggedIn(){
		return isLoggedIn;
	}
	
	public void setIsLoggedIn(boolean isLoggedIn){
		this.isLoggedIn = isLoggedIn;
	}
}
