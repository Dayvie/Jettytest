package jsf;

import java.io.Serializable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import util.Utility;


public class CalcBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	String result = "Your result";
	
	public CalcBean(){
	}

	public void calculate(String expressionToCalculate, String x, String y) {
		System.out.println(expressionToCalculate);
		if(x.isEmpty())
			x = "0";
		if(y.isEmpty())
			y = "0";
		double xResult, yResult;
		try {
			xResult = new ExpressionBuilder(x).build().evaluate();
		} catch (Exception e) {
			xResult = 0;
		}
		try {
			yResult = new ExpressionBuilder(y).build().evaluate();
		} catch (Exception e) {
			yResult = 0;
		}
		if(expressionToCalculate.isEmpty())
			expressionToCalculate = "0";
		Expression e = new ExpressionBuilder(expressionToCalculate)
		        .variables("x", "y")
		        .build()
		        .setVariable("x", xResult)
		        .setVariable("y", yResult);
		result = String.valueOf(e.evaluate());
	}
	
	public String getResult(){
		return result;
	}
	
	public void setResult(String result){
		this.result = result;
	}
}
