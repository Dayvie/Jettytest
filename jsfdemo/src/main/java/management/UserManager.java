package management;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import util.Utility;

public class UserManager {
	
	private static Map<String, String> currentUsers = new ConcurrentHashMap<>();
	
	public static void addUser(String userName){
		currentUsers.put(Utility.getSessionID(), userName);
	}
	
	public static String getUser(){
		return currentUsers.get(Utility.getSessionID());
	}
}
