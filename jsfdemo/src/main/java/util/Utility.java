package util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class Utility {
	
	public static String getSessionID(){
		FacesContext fCtx = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		return session.getId();
	}

}
