package controller;

public interface LoginController {
	public boolean authenticate(String user, String password);
	public String getUserName();
}
