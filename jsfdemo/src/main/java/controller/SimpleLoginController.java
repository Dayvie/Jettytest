/**
 * 
 */
package controller;

import java.io.Serializable;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import management.UserManager;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import util.Utility;

/**
 * @author dayvie
 *
 */
public class SimpleLoginController implements LoginController, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3120170730819457960L;

	/* (non-Javadoc)
	 * @see Controller.LoginController#authenticate(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean authenticate(String user, String password) {
		if(user.toLowerCase().equals("dayvie")){
			UserManager.addUser("dayvie");
			return true;
		}
		return false;
	}
	
	public String getUserName(){
		String userName = UserManager.getUser();
		if(userName != null){
			return userName;
		}
		return "You are not logged in!";
	}
	
	
	
}
